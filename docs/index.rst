.. _swh-alter:

.. include:: README.rst

.. toctree::
   :maxdepth: 1
   :caption: Overview

   removal-algorithm

.. only:: standalone_package_doc

   Indices and tables
   ------------------

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
